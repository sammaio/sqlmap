from ubuntu


#Install base
RUN apt-get update && apt-get install wget git python3-pip -y
RUN pip3 install --upgrade pip
RUN pip3 install xmltodict amqplib


#Instal sqlmap
WORKDIR /opt/
RUN git clone --depth 1 https://github.com/sqlmapproject/sqlmap.git sqlmap

COPY  ./code /code


CMD nikto
